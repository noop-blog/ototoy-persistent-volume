// ==UserScript==
// @name         OTOTOY Persistent Volume
// @namespace    https://noop.ovh/
// @version      1.0
// @description  Save the volume of https://ototoy.jp
// @author       Benny S
// @match        https://ototoy.jp/_/default/p/*
// @run-at       document-idle
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==
//
// MIT License
//
// Copyright (c) 2021 Benny S
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


function set_volume(vol_element, vol) {
    const DEFAULT_VOL = 0.1

    if (vol === undefined){
        vol = DEFAULT_VOL
    }

    vol_element.value = vol
    vol_element.dispatchEvent(new Event('input'))
}

function save_volume() {
    const vol_storage = 'Ototoy_vol'
    const vol_element = document.querySelector('.volume > input:nth-child(1)')
    const vol = vol_element.value.toString()

    const result = GM_setValue(vol_storage, vol)
}

(async function() {
    'use strict';
    const vol_storage = 'Ototoy_vol'

    const vol_element = document.querySelector('.volume > input:nth-child(1)')
    const vol = await GM_getValue(vol_storage)

    set_volume(vol_element, vol)
    vol_element.addEventListener('input', save_volume)
})()
